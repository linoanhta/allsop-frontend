import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { GiaHoaLayoutComponent } from './giahoa-layout/giahoa-layout.component';

const routes: Routes = [
  {
    path: '',
    component: GiaHoaLayoutComponent,
    children: [
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'news/:id',
        loadChildren: () => import('./news-detail/news-detail.module').then(m => m.NewsDetailModule)
      },
      {
        path: 'news/:slug-:id' + '.html',
        loadChildren: () => import('./news-detail/news-detail.module').then(m => m.NewsDetailModule)
      },
      {
        path: 'news',
        loadChildren: () => import('./news/news.module').then(m => m.NewsModule)
      },
      {
        path: 'page-under-construction',
        loadChildren: () => import('./page-under-construction/page-under-construction.module').then(m => m.PageUnderConstructionModule)
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
  })], providers: [
    { provide: APP_BASE_HREF, useValue: environment.baseHref }
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
