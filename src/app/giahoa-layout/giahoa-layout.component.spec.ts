import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GiaHoaLayoutComponent } from './giahoa-layout.component';

describe('giahoaLayoutComponent', () => {
  let component: GiaHoaLayoutComponent;
  let fixture: ComponentFixture<GiaHoaLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GiaHoaLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GiaHoaLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
