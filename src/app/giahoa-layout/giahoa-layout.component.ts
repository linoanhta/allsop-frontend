import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'app-giahoa-layout',
  templateUrl: './giahoa-layout.component.html',
  styleUrls: ['./giahoa-layout.component.scss']
})
export class GiaHoaLayoutComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  // viewCart() {
  //   this.router.navigate(['cart']);
  // }
}
