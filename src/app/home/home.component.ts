import { Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { fadeInUp400ms } from 'src/@loka/services/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@loka/services/animations/stagger.animation';

@UntilDestroy()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
})
export class HomeComponent {}
