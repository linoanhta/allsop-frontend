import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NewsDetailComponent } from './news-detail.component';

const routes: any = [
  {
    path: '',
    component: NewsDetailComponent,
    data: {
      toolbarShadowEnabled: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsDetailRoutingModule {
}
