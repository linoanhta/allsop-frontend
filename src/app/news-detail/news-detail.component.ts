import { NewsService } from '../services/news/news.service';
import { News } from '../shared/news.model';
import { Component, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { fadeInUp400ms } from 'src/@loka/services/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@loka/services/animations/stagger.animation';
import { filter, Observable, of, ReplaySubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroup } from '@angular/forms';
@UntilDestroy()
@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
})
export class NewsDetailComponent implements OnInit {
  currentNewsDetail: any;
  currentNewsDetailId: any = '';

  constructor(
    private newsService: NewsService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: any) => {
      this.currentNewsDetailId = params.get('id') || '';
    });
  }
}
