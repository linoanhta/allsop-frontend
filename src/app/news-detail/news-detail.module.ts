import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NewsDetailRoutingModule } from './news-detail-routing.module';
import { NewsDetailComponent } from './news-detail.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  declarations: [NewsDetailComponent],
  imports: [
    CommonModule,
    NewsDetailRoutingModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatInputModule,
    MatFormFieldModule
  ]
})
export class NewsDetailModule {
}
