import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { fadeInUp400ms } from 'src/@loka/services/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@loka/services/animations/stagger.animation';
import { NewsService } from '../services/news/news.service';
import { Observable, of, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
})
export class NewsComponent implements OnInit {

  newsForm!: FormGroup;

  constructor(
    private newsService: NewsService,
    private snackBar: MatSnackBar) {  }

  ngOnInit(): void {
    this.newsForm = new FormGroup(
      { catalog: new FormControl('', Validators.required) },
    );

  }

  get f() { return this.newsForm.controls; }
}
