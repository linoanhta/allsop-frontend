import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageUnderConstructionComponent } from './page-under-construction.component';

const routes: any = [
  {
    path: '',
    component: PageUnderConstructionComponent,
    data: {
      toolbarShadowEnabled: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageUnderConstructionRoutingModule {
}
