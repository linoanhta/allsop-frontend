import { Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { fadeInUp400ms } from 'src/@loka/services/animations/fade-in-up.animation';
import { stagger40ms } from 'src/@loka/services/animations/stagger.animation';

@UntilDestroy()
@Component({
  selector: 'app-page-under-construction',
  templateUrl: './page-under-construction.component.html',
  styleUrls: ['./page-under-construction.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
})
export class PageUnderConstructionComponent {}
