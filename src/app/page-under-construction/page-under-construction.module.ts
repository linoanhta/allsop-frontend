import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageUnderConstructionRoutingModule } from './page-under-construction-routing.module';
import { PageUnderConstructionComponent } from './page-under-construction.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
@NgModule({
  declarations: [PageUnderConstructionComponent],
  imports: [
    CommonModule,
    PageUnderConstructionRoutingModule,
    ReactiveFormsModule ,
    MatSnackBarModule,
    MatInputModule,
    FormsModule,
    MatFormFieldModule
  ]
})
export class PageUnderConstructionModule {
}
