export class News {
  id: number;
  title: string;
  thumbnail: string;
  excerpt: string;
  content: string;
  category: { id: string, name: string };
  views: number;
  keywords: string;
  is_public: number;
  published_on: string;
  published_by: string;
  modified_on: string;
  modified_by: string;
  created_on: string;
  created_by: string;


  constructor(news: any) {
    this.id = news.id;
    this.title = news.title;
    this.thumbnail = news.thumbnail;
    this.excerpt = news.excerpt;
    this.content = news.content;
    this.category = news.category;
    this.views = news.views;
    this.keywords = news.keywords;
    this.is_public = news.is_public;
    this.published_on = news.published_on;
    this.published_by = news.published_by;
    this.modified_on = news.modified_on;
    this.published_by = news.published_by;
    this.modified_by = news.modified_by;
    this.created_on = news.created_on;
    this.created_by = news.created_by;
  }
}
