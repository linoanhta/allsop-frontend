export const environment = {
  production: true,
  baseHref: '/',
  baseApi: 'https://t50ud7k29c.execute-api.ap-southeast-1.amazonaws.com/prod'
};
