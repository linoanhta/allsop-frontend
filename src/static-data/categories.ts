export const categories = [
  {
    id: 1,
    name: 'Meat & Poultry',
    description: 'Clean eating plan',
    image: 'meat-poultry.jpg',
  },
  {
    id: 2,
    name: 'Fruit & Vegetables',
    description: 'Fresh eating journey',
    image: 'fruit-vege.jpg',
  },
  {
    id: 3,
    name: 'Drinks',
    description: 'Daily boost up',
    image: 'drinks.jpg',
  },
  {
    id: 4,
    name: 'Confectionary & Desserts',
    description: 'Confectionary & Desserts ',
    image: 'desserts.jpg',
  },
  {
    id: 5,
    name: 'Baking/Cooking Ingredients',
    description: 'Baking/Cooking Ingredients',
    image: 'baking.jpg',
  },
  {
    id: 6,
    name: 'Miscellaneous Items',
    description: 'Miscellaneous Items',
    image: 'misc.jpg',
  }
]
